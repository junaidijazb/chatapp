package com.junaid.chatdemo.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.junaid.chatdemo.Collections
import com.junaid.chatdemo.Collections.collectionChatWith
import com.junaid.chatdemo.Collections.collectionConversations
import com.junaid.chatdemo.Collections.collectionUsers
import com.junaid.chatdemo.R
import com.junaid.chatdemo.Utils
import com.junaid.chatdemo.adapters.ConversationAdapter
import com.junaid.chatdemo.firebase.FireBaseServices
import com.junaid.chatdemo.formattedText
import com.junaid.chatdemo.models.Chats
import com.junaid.chatdemo.models.Conversation
import com.junaid.chatdemo.models.Message
import com.junaid.chatdemo.models.User
import com.junaid.chatdemo.ui.MainActivity.Companion.currentUser
import kotlinx.android.synthetic.main.activity_converstaion.*


class ConversationActivity : AppCompatActivity() {

    private val TAG = "ConversationActivity"
    lateinit var sentToUser: User
    private var chatId = ""
    private var isNewChat = false
    lateinit var adapter : ConversationAdapter

    companion object {
        fun startActivity(context: Context, participant: User) {
            context.startActivity(Intent(context, ConversationActivity::class.java).apply {
                putExtra("participant", participant)
            })
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_converstaion)
        sentToUser = intent.extras?.get("participant") as User

        tvChatName.text = sentToUser.name
        chatId = setOneToOneChat(currentUser.userId, sentToUser.userId)

        etMessage.requestFocus()
        getConversation()
        applyListeners()
        buildRecyclerView()

    }

    private fun buildRecyclerView() {
        val _layoutManager = LinearLayoutManager(this)
        _layoutManager.reverseLayout = true
        _layoutManager.stackFromEnd = true
        rvConversation.layoutManager = _layoutManager

        val list = ArrayList<Conversation>()
        list.add(Conversation("hello", currentUser.email, ""))
        list.add(Conversation("yooo", currentUser.email, ""))
        list.add(Conversation("helloasdasdasdasdasdasdasdasdadasdada", "", ""))
        list.add(Conversation("trtr", "", ""))
        list.add(Conversation("hello", currentUser.email, ""))
        list.add(Conversation("hello", currentUser.email, ""))
        list.add(Conversation("hello", currentUser.email, ""))
        list.add(Conversation("hello", currentUser.email, ""))
        list.add(Conversation("hello", currentUser.email, ""))
        list.add(Conversation("hello", "", ""))
        adapter = ConversationAdapter(list)
        rvConversation.scrollToPosition(adapter.itemCount)
        rvConversation.adapter = adapter

        rvConversation.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom < oldBottom) {
                rvConversation.postDelayed(Runnable {
                    rvConversation.smoothScrollToPosition(
                        adapter.itemCount
                    )
                }, 100)
            }
        }

    }

    private fun applyListeners() {
        btnSend.setOnClickListener {
            sendMsg(etMessage.formattedText())
        }
    }

    private fun sendMsg(msg: String) {
        if (msg != "") {
            Log.d(TAG, "sendMsg: ")


            val message = Message(
                msg,
                sentToUser.userId,
                currentUser.userId,
                currentUser.email,
                Utils.getTimeStamp()
            )

            FireBaseServices.fireStore.collection(collectionConversations)
                .document(chatId)
                .collection(Collections.collectionMessages)
                .add(message)
                .addOnSuccessListener {
                    updateUsersChat(msg)
                }
        }
    }

    private fun updateUsersChat(lastMsg: String) {
        val chat =
            Chats(
                lastMsg,
                Utils.getTimeStamp(),
                sentToUser,
                currentUser.email
            )
        FireBaseServices.fireStore.collection(collectionUsers)
            .document(currentUser.userId)
            .collection(collectionChatWith)
            .document(sentToUser.userId)
            .set(chat)

        chat.particepent = currentUser

        FireBaseServices.fireStore.collection(collectionUsers)
            .document(sentToUser.userId)
            .collection(collectionChatWith)
            .document(currentUser.userId)
            .set(chat)
    }

    private fun getConversation() {
        FireBaseServices.fireStore.collection(collectionConversations).document(chatId)
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    it.result?.data ?: run { isNewChat = true }

                    Log.d(TAG, "getConversation: $isNewChat")
                }
            }
    }

    private fun setOneToOneChat(uid1: String, uid2: String): String {
        return if (uid1 < uid2) {
            uid1 + uid2
        } else {
            uid2 + uid1
        }
    }
}
