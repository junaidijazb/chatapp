package com.junaid.chatdemo.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.junaid.chatdemo.Collections
import com.junaid.chatdemo.R
import com.junaid.chatdemo.isEmpty
import com.junaid.chatdemo.isValidEmail
import com.junaid.chatdemo.models.User
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private val TAG = "SignUpActivity"

    private lateinit var pb: KProgressHUD
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var db: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        setListeners()

        firebaseAuth = FirebaseAuth.getInstance()
        db = Firebase.firestore
        pb = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(false)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)


    }

    private fun setListeners() {
        btnSignUp.setOnClickListener {
            if (isFormValid()) {
                signUp(etEmail.text.toString().trim(), etPassword.text.toString().trim())
            }
        }

        tvAlreadyHaveAcc.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    private fun signUp(userName: String, password: String) {
        Log.d(TAG, "signUp: ")
        pb.show()
        firebaseAuth.createUserWithEmailAndPassword(userName, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = User(
                        etFullName.text.toString(),
                        etPassword.text.toString(),
                        etEmail.text.toString(),
                        firebaseAuth.currentUser!!.uid
                    )

                    saveUserToDataBase(user)
                } else {
                    pb.dismiss()
                    Toast.makeText(
                        this,
                        task.exception?.localizedMessage.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e(TAG, "Error signingUp document", task.exception)
                }
            }
    }

    private fun saveUserToDataBase(user: User) {
        Log.d(TAG, "saveUserToDataBase: ")

        db.collection(Collections.collectionUsers).document(user.userId).set(user)
            .addOnSuccessListener { docRefs ->
                pb.dismiss()
                clearFields()
                Toast.makeText(this, "Successfully Registered", Toast.LENGTH_SHORT).show()
                Log.d(TAG, "DocumentSnapshot added with ID: $docRefs")
                signInUser(user)
            }
            .addOnFailureListener {
                firebaseAuth.currentUser?.delete()
                pb.dismiss()
                Toast.makeText(this, it.localizedMessage?.toString(), Toast.LENGTH_SHORT).show()
                Log.e(TAG, "Error adding document", it)
            }
    }

    private fun signInUser(user: User) {
        Log.d(TAG, "signInUser: ")
        val intent = Intent(this, MainActivity::class.java).apply {
            putExtra("user", user)
        }
        startActivity(intent)
        finish()
    }

    private fun clearFields() {
        etFullName.setText("")
        etEmail.setText("")
        etCPassword.setText("")
        etPassword.setText("")
    }

    private fun isFormValid(): Boolean {


        Log.d(TAG, "isFormValid: ")
        if (etFullName.isEmpty()) {
            etFullName.error = "Full name required"
            return false
        }

        if (etEmail.isEmpty()) {
            etEmail.error = "Email required"
            return false
        }

        if (!etEmail.text.toString().isValidEmail()) {
            etEmail.error = "Please enter valid email"
            return false
        }

        if (etPassword.isEmpty()) {
            etPassword.error = "Password required"
            return false
        }

        if (etPassword.text.trim().length < 6) {
            etPassword.error = "Password length must be greater then 5"
            return false
        }

        if (etPassword.text.trim() != etCPassword.text.trim()) {
            etPassword.error = "Password not matched"
            etCPassword.setText("")
            return false
        }

        return true
    }
}
