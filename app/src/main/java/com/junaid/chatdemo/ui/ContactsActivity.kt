package com.junaid.chatdemo.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.junaid.chatdemo.R
import com.junaid.chatdemo.adapters.SelectContactsAdapter
import com.junaid.chatdemo.changeToolbarFont
import com.junaid.chatdemo.models.User
import kotlinx.android.synthetic.main.activity_contacts.*


class ContactsActivity : AppCompatActivity() {

    private val TAG = "ContactsActivity"
    lateinit var db: FirebaseFirestore
    private var listOfContacts = ArrayList<User>()
    private lateinit var adapter: SelectContactsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
        setSupportActionBar(tbContacts)
        tbContacts.changeToolbarFont()
        tbContacts.setNavigationOnClickListener {
            onBackPressed()
        }

        db = Firebase.firestore
        buildRecyclerView()
        getAllContacts()


    }

    private fun buildRecyclerView() {
        rvContacts.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.HORIZONTAL
            )
        )
        rvContacts.layoutManager = LinearLayoutManager(this)
        adapter = SelectContactsAdapter()
        rvContacts.adapter = adapter

        adapter.onItemClicked = {
            Log.d(TAG, "buildRecyclerView: ")
            ConversationActivity.startActivity(this,it)
        }

    }

    private fun getAllContacts() {
        pbContacts.visibility = View.VISIBLE
        Log.d(TAG, "getAllContacts: ")
        listOfContacts.clear()
        db.collection("users").get().addOnCompleteListener {
            pbContacts.visibility = View.GONE
            if (it.isSuccessful) {
                val temp = it.result?.documents
                if (temp != null) {
                    for (item in temp) {
                        val user = item.toObject<User>()
                        listOfContacts.add(user!!)
                        Log.d(TAG, "getAllContacts: $user")
                    }

                    listOfContacts =
                        ArrayList(listOfContacts.filter { item -> item.userId != MainActivity.currentUser.userId })
                    adapter.updateList(listOfContacts)
                }else{
                    //todo show no users msgs
                }
            } else {
                Toast.makeText(this, "${it.exception?.localizedMessage}", Toast.LENGTH_SHORT).show()
            }
        }

    }
 
}
