package com.junaid.chatdemo.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ktx.toObject
import com.junaid.chatdemo.R
import com.junaid.chatdemo.adapters.ChatsAdapter
import com.junaid.chatdemo.changeToolbarFont
import com.junaid.chatdemo.firebase.FireBaseServices
import com.junaid.chatdemo.models.Chats
import com.junaid.chatdemo.models.User
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"
    var listChats = ArrayList<Chats>()
    lateinit var chatsAdapter: ChatsAdapter

    companion object {
        lateinit var currentUser: User
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(tbMail)
        tbMail.changeToolbarFont()

        val user = intent.extras?.get("user") as User
        currentUser = user
        Log.d(TAG, "onCreate: $user")

        buildRecyclerView()
        setClickListeners()
        getChats()

        Toast.makeText(this, "Logged in as ${currentUser.name}", Toast.LENGTH_SHORT).show()

    }

    private fun getChats() {
        listChats.clear()
        FireBaseServices.getChatDocumentRef().get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val temp = it.result?.documents
                    if (temp != null) {
                        for (item in temp) {
                            listChats.add(item.toObject<Chats>()!!)
                        }
                        chatsAdapter.updateList(listChats)
                    }
                } else {
                    //todo chats msg
                }

                setChatChangeListener()
            }
    }

    private fun setChatChangeListener() {
        FireBaseServices.getChatDocumentRef()
            .addSnapshotListener { querySnapshot, exeption ->
                if (exeption != null) {
                    Log.d(TAG, "setChatChangeListener: ", exeption)
                    return@addSnapshotListener
                }
                if (querySnapshot != null) {
                    if (querySnapshot.documents.isNotEmpty()) {
                        val chat = querySnapshot.documents[0].toObject<Chats>()
                        chatsAdapter.addChat(chat!!)
                    }
                } else {
                    Log.d(TAG, "Current data: null")
                }
            }
    }

    private fun buildRecyclerView() {
        rvChats.layoutManager = LinearLayoutManager(this)
        chatsAdapter = ChatsAdapter()
        rvChats.adapter = chatsAdapter

        chatsAdapter.onItemClicked = {
            ConversationActivity.startActivity(this, it)
        }

    }

    private fun setClickListeners() {
        fabNewSms.setOnClickListener {
            startActivity(Intent(this, ContactsActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.tb_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuLogout) {
            FireBaseServices.firebaseAuth.signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        return true
    }
}
