package com.junaid.chatdemo.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.junaid.chatdemo.Collections.collectionChatWith
import com.junaid.chatdemo.Collections.collectionUsers
import com.junaid.chatdemo.R
import com.junaid.chatdemo.isEmpty
import com.junaid.chatdemo.models.User
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private val TAG = "LoginActivity"
    private lateinit var pb: KProgressHUD
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        Log.d(TAG, "onCreate: ")
        firebaseAuth = FirebaseAuth.getInstance()
        db = Firebase.firestore

        setListeners()
        pb = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(false)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)

        if (firebaseAuth.currentUser != null) {
            getUserData(firebaseAuth.uid!!)
        }

    }

    private fun setListeners() {
        Log.d(TAG, "setListeners: ")
        btnLogin.setOnClickListener {
            if (isFormValid()) {
                signInUser(etEmail.text.toString().trim(), etPassword.text.toString().trim())
            }
        }

        tvCreateNewAccount.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
            finish()
        }
    }

    private fun signInUser(email: String, pass: String) {
        pb.show()
        Log.d(TAG, "signInUser: ")
        firebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                getUserData(firebaseAuth.uid!!)
                Log.d(TAG, "signInUser: loggedIn")
            } else {
                pb.dismiss()
                Log.d(TAG, "signInUser: ${task.exception?.localizedMessage}")
            }
        }

    }

    private fun getUserData(id: String) {
        Log.d(TAG, "getUserData: ")
        if (!pb.isShowing)
            pb.show()

        db.collection(collectionUsers).document(id).get().addOnCompleteListener { task ->
            pb.dismiss()
            if (task.isSuccessful) {
                val user = task.result?.toObject<User>()
                val intent = Intent(this, MainActivity::class.java).apply {
                    putExtra("user", user)
                }
                startActivity(intent)
                finish()
            } else {
                Log.d(TAG, "getUserData: ${task.exception?.localizedMessage}")
                Toast.makeText(this, task.exception?.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun isFormValid(): Boolean {
        Log.d(TAG, "isFormValid: ")
        if (etEmail.isEmpty()) {
            etEmail.error = "Email required"
            return false
        }
        if (etPassword.isEmpty()) {
            etPassword.error = "Password required"
            return false
        }
        return true
    }
}

