package com.junaid.chatdemo.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.junaid.chatdemo.R
import com.junaid.chatdemo.inflate
import com.junaid.chatdemo.models.Conversation
import com.junaid.chatdemo.models.User
import com.junaid.chatdemo.ui.MainActivity
import kotlinx.android.synthetic.main.list_item_chat_out.view.*


class ConversationAdapter(var conversationList : ArrayList<Conversation>) : RecyclerView.Adapter<ConversationAdapter.BaseViewHolder<*>>() {

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T)
    }


    companion object {
        private const val TYPE_SMS_IN = 0
        private const val TYPE_SMS_OUT = 1

    }

//    private var conversationList: ArrayList<Conversation> = ArrayList()
    var onItemClicked: ((User) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            TYPE_SMS_IN -> {
                val inflatedView = parent.inflate(R.layout.list_item_chat_in, false)
                SmsInViewHolder(inflatedView)
            }
            TYPE_SMS_OUT -> {
                val inflatedView = parent.inflate(R.layout.list_item_chat_out, false)
                SmsOutViewHolder(inflatedView)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }

    }

    override fun getItemCount() = conversationList.size

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val currentItem = conversationList[position]
        when(holder)
        {
           is SmsInViewHolder -> holder.bind(currentItem)
            else -> (holder as SmsOutViewHolder).bind(currentItem)
        }

    }

    override fun getItemViewType(position: Int) : Int
    {
        val comparable = conversationList[position]
        return if(comparable.participent == MainActivity.currentUser.email)
            TYPE_SMS_IN
        else
            TYPE_SMS_OUT

    }

    override fun getItemId(position: Int) = position.toLong()

    fun updateList(list: ArrayList<Conversation>) {
        conversationList.clear()
        conversationList.addAll(list)
        notifyDataSetChanged()
    }


    //1
     class SmsInViewHolder(v: View) : BaseViewHolder<Conversation>(v) {

        val sms: TextView = v.tvSms
        val tvTimestamp = v.tvSmsTime
        override fun bind(item: Conversation) {
            sms.text = item.sms
        }
    }

    //2
    class SmsOutViewHolder(v: View) : BaseViewHolder<Conversation>(v) {
        val sms: TextView = v.tvSms
        val tvTimestamp = v.tvSmsTime
        override fun bind(item: Conversation) {
            sms.text = item.sms
        }
    }

}