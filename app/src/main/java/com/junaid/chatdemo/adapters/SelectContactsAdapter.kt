package com.junaid.chatdemo.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.junaid.chatdemo.R
import com.junaid.chatdemo.inflate
import com.junaid.chatdemo.models.User
import kotlinx.android.synthetic.main.list_item_contact.view.*

class SelectContactsAdapter : RecyclerView.Adapter<SelectContactsAdapter.ItemViewHolder>() {

    private var usersList : ArrayList<User> = ArrayList()
    var onItemClicked : ((User) -> Unit)? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflatedView = parent.inflate(R.layout.list_item_contact, false)
        return ItemViewHolder(inflatedView)
    }

    override fun getItemCount() = usersList.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = usersList[position]
        currentItem.apply {
//            holder.dp.setImageDrawable(icon)
            holder.chatName.text = name
            holder.tvChatDis.text = email
            holder.bgMainContacts.setOnClickListener {
                onItemClicked?.invoke(this)
            }
        }
    }

    override fun getItemViewType(position: Int) = position

    override fun getItemId(position: Int) = position.toLong()

    fun updateList(list: ArrayList<User>) {
        usersList.clear()
        usersList.addAll(list)
        notifyDataSetChanged()
    }

    //1
    class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val dp: ImageView = v.ivDp
        val chatName: TextView = v.tvChatName
        val tvChatDis = v.tvChatDis
        val bgMainContacts = v.bgMainContacts
    }

}