package com.junaid.chatdemo.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.junaid.chatdemo.R
import com.junaid.chatdemo.inflate
import com.junaid.chatdemo.models.Chats
import com.junaid.chatdemo.models.User
import com.junaid.chatdemo.ui.MainActivity
import kotlinx.android.synthetic.main.list_item_contact.view.*

class ChatsAdapter : RecyclerView.Adapter<ChatsAdapter.ItemViewHolder>() {

    private var chatsList: ArrayList<Chats> = ArrayList()
    var onItemClicked: ((User) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflatedView = parent.inflate(R.layout.list_item_contact, false)
        return ItemViewHolder(inflatedView)
    }

    override fun getItemCount() = chatsList.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = chatsList[position]
        currentItem.apply {
//            holder.dp.setImageDrawable(icon)
            lastMsg = if (lastMsgBy == MainActivity.currentUser.email)
                "you:$lastMsg"
            else {
                "${lastMsgBy}:${lastMsg}"
            }

            holder.chatName.text = particepent.name
            holder.tvChatDis.text = lastMsg
            holder.bgMainContacts.setOnClickListener {
                onItemClicked?.invoke(particepent)
            }

            holder.tvChatTime.visibility = View.VISIBLE
        }
    }

    override fun getItemViewType(position: Int) = position

    override fun getItemId(position: Int) = position.toLong()

    fun updateList(list: ArrayList<Chats>) {
        chatsList.clear()
        chatsList.addAll(list)
        notifyDataSetChanged()
    }

    fun addChat(chat: Chats) {
        if (chatsList.contains(chat)) {
            val index = chatsList.indexOf(chat)
            chatsList[index] = chat
            notifyItemChanged(index)
        } else {
            chatsList.add(chat)
            notifyItemInserted(itemCount+1)
        }

    }

    //1
    class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val dp: ImageView = v.ivDp
        val chatName: TextView = v.tvChatName
        val tvChatDis = v.tvChatDis
        val tvChatTime = v.tvChatTime
        val bgMainContacts = v.bgMainContacts
    }

}