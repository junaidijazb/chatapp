package com.junaid.chatdemo

class Utils {

    companion object{
        fun getTimeStamp() =  (System.currentTimeMillis() / 1000).toString()
    }
}