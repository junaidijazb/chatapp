package com.junaid.chatdemo

object Collections {
    const val collectionConversations = "conversation"
    const val collectionUsers = "users"
    const val collectionMessages = "messages"
    const val collectionChats = "chats"
    const val collectionChatWith = "chatsWith"
}