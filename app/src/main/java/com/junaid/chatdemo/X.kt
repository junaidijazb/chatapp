package com.junaid.chatdemo

import android.graphics.Typeface
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.Toolbar

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun EditText.isEmpty() : Boolean
{
    return this.text.toString().trim() == ""
}

fun EditText.formattedText() : String
{
    return this.text.toString().trim()
}

fun CharSequence.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun Toolbar.changeToolbarFont(){
    for (i in 0 until childCount) {
        val view = getChildAt(i)
        if (view is TextView && view.text == title) {
            val typeface = Typeface.create("font/aspira_bold.ttf",Typeface.NORMAL )
            view.typeface = typeface
            break
        }
    }
}