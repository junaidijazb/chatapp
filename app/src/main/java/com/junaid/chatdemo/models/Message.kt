package com.junaid.chatdemo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Message(
    var messageText: String,
    var receivedBy: String,
    var sentBy: String,
    var sentByEmail: String,
    var timeStamp: String
) : Parcelable