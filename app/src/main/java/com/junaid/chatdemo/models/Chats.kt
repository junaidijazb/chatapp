package com.junaid.chatdemo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Chats(
    var lastMsg : String,
    var timeStamp : String,
    var particepent : User,
    var lastMsgBy : String
    ) : Parcelable
{
    constructor() : this("","",User() ,"")

    override fun equals(obj: Any?): Boolean {
        var result = false
        if (obj == null ) {
            result = false
        } else {
            val chat: Chats = obj as Chats
            if (this.particepent == chat.particepent) {
                result = true
            }
        }
        return result
    }
}