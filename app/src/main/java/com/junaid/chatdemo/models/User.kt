package com.junaid.chatdemo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(var name : String, var password : String, var email : String,var userId : String) :
    Parcelable
{
    constructor() : this("","","","")
}
