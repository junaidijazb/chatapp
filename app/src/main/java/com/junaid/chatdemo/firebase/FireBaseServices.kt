package com.junaid.chatdemo.firebase

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.junaid.chatdemo.Collections
import com.junaid.chatdemo.ui.MainActivity

object FireBaseServices {
    val firebaseAuth = FirebaseAuth.getInstance()
    val fireStore = Firebase.firestore

    fun getChatDocumentRef(): CollectionReference {
        return FireBaseServices.fireStore.collection(Collections.collectionUsers)
            .document(MainActivity.currentUser.userId)
            .collection(Collections.collectionChatWith)
    }
}